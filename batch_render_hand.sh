#!/bin/bash
(( $# != 3 )) && echo "usage: $0 <radius> <cams> <odir>" && exit

radius=$1
cams=$2
odir=$3

mkdir -p $odir/{rgb,depth}

blender -b blender_hand/hand.blend -P blender_hand/batch_render_hand.py -- $radius $cams $odir/rgb/
blender -b blender_hand/depth.blend -P blender_hand/batch_render_hand.py -- $radius $cams $odir/depth/

