Overview
========

This repository demonstrates batch rendering a human hand using blender. The
model is taken from libhand (http://www.libhand.org) and adapted to make a
pointing gesture.

Install
=======

You need Python and Blender.

Usage
=====

    $ ./batch_render_hand.sh <radius> <cams> <odir>

Here the radius is the distance from the camera to the hand. The cams file is a
space-separated file consisting of azimuth-altitude pairs that may be created
using utils.py. E.g.

    $ python
    ... 
    >>> import utils
    >>> import numpy as np
    >>> cams = utils.make_cams(16, 4) # 16 (4) different azimuths (altitudes)
    >>> np.savetxt('cams.tab', cams)

Finally odir is the output directory where the different views will be written
to. Considering a cams file as described above, the following command will
create 128 (64 in rgb, 64 in depth) images in the directory out.

    $ ./batch_render_hand.sh 7 cams.tab out

License
=======

See blender_hand/license.txt for the hand models license. The rest is WTFPLv2.

