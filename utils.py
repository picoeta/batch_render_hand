import numpy as np

def make_cams(nazimuth, naltitude, max_altitude=np.pi/4):
    azimuths = np.linspace(0, 2*np.pi, nazimuth+1)[:-1]
    altitudes = np.linspace(0, max_altitude, naltitude+1)[:-1]

    cams = []
    for azimuth in azimuths:
        for altitude in altitudes:
            cams.append([azimuth, altitude])

    return np.array(cams)

