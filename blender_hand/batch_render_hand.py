import numpy as np
import bpy
import sys

args = sys.argv[sys.argv.index('--') + 1:] # slightly hacky

radius = float(args[0])
cams_path = args[1]
angles = np.loadtxt(cams_path)
odir = args[2]

scene = bpy.data.scenes['Scene']
cam = scene.camera

for i, (azimuth, altitude) in enumerate(angles):
    t = radius * np.cos(altitude)

    x = t * np.cos(azimuth)
    y = t * np.sin(azimuth)
    z = radius * np.sin(altitude)

    cam.rotation_euler = np.pi/2 - altitude, 0, np.pi/2 + azimuth # x-y-z
    cam.location = x, y, z

    #print('loc: ' + str(cam.location) + ', euler_xyz = ' + str(cam.rotation_euler))

    scene.render.filepath = odir + str(i) + '.png'
    bpy.ops.render.render(write_still=True) 

